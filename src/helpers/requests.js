import axios from 'axios';

export const getProjects = async (pat) => {
    return await axios.get('https://gitlab.com/api/v4/projects?visibility=private', { headers: { "PRIVATE-TOKEN": pat } });
}

export const getProjectBranches = async (pat, id) => {
    return await axios.get(`https://gitlab.com/api/v4/projects/${id}/repository/branches`, { headers: { "PRIVATE-TOKEN": pat } });
}

export const getBranchCommits = async (pat, id, branch) => {
    return await axios.get(`https://gitlab.com/api/v4/projects/${id}/repository/commits?ref_name=${branch}`, { headers: { "PRIVATE-TOKEN": pat } });
}

export const getComments = async (pat, id, sha) => {
    return await axios.get(`https://gitlab.com/api/v4/projects/${id}/repository/commits/${sha}/comments`, { headers: { "PRIVATE-TOKEN": pat } });
}

export const postComment = async (pat, id, sha, note) => {
    return await axios.post(`https://gitlab.com/api/v4/projects/${id}/repository/commits/${sha}/comments`, { headers: { "PRIVATE-TOKEN": pat }, form: { note } });
}

export const getProjectMRs = async (pat, id) => {
    return await axios.get(`https://gitlab.com/api/v4/projects/${id}/merge_requests`, { headers: { "PRIVATE-TOKEN": pat } });
}

export const getUser = async (pat) => {
    return await axios.get(`https://gitlab.com/api/v4/user`, { headers: { "PRIVATE-TOKEN": pat } });
}

export const merge = async (pat, id, iid) => {
    return await axios.put(`https://gitlab.com/api/v4/projects/${id}/merge_requests/${iid}/merge`, { headers: { "PRIVATE-TOKEN": pat } });
}

export const getIssues = async (pat, id) => {
    return await axios.get(`https://gitlab.com/api/v4/projects/${id}/issues`, { headers: { "PRIVATE-TOKEN": pat } });
}

export const editIssue = async (pat, id, iid, params) => {
    return await axios.put(`https://gitlab.com/api/v4/projects/${id}/issues/${iid}`, { params, headers: { "PRIVATE-TOKEN": pat } });
}
