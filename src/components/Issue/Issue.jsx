import { Button, TextField } from "@mui/material";
import { useState } from "react";
import { editIssue } from '../../helpers/requests';

function Issue({
  title,
  description,
  projectId,
  id
}) {
  const [params, setParams] = useState({
    assignee_id: '',
    assignee_ids: '',
    confidential: '',
    created_at: '',
    description: '',
    discussion_locked: '',
    due_date: '',
    issue_type: '',
    labels: '',
    milestone_id: '',
    state_event: '',
    title: ''
  })

  return (
    <div id="container">
      <h2>{title}</h2>
      <h4>{description}</h4>

      <TextField label='assignee_id' variant="standard" value={params.assignee_id} onChange={(e) => {
        setParams({assignee_id: e.target.value})
      }}></TextField>
      <TextField label='assignee_ids' variant="standard" value={params.assignee_ids} onChange={(e) => {
        setParams({assignee_ids: e.target.value})
      }}></TextField>
      <TextField label='confidential' variant="standard" value={params.confidential} onChange={(e) => {
        setParams({confidential: e.target.value})
      }}></TextField>
      <TextField label='created_at' variant="standard" value={params.created_at} onChange={(e) => {
        setParams({created_at: e.target.value})
      }}></TextField>
      <TextField label='description' variant="standard" value={params.description} onChange={(e) => {
        setParams({description: e.target.value})
      }}></TextField>
      <TextField label='discussion_locked' variant="standard" value={params.discussion_locked} onChange={(e) => {
        setParams({discussion_locked: e.target.value})
      }}></TextField>
      <TextField label='due_date' variant="standard" value={params.due_date} onChange={(e) => {
        setParams({due_date: e.target.value})
      }}></TextField>
      <TextField label='issue_type' variant="standard" value={params.issue_type} onChange={(e) => {
        setParams({issue_type: e.target.value})
      }}></TextField>
      <TextField label='labels' variant="standard" value={params.labels} onChange={(e) => {
        setParams({labels: e.target.value})
      }}></TextField>
      <TextField label='milestone_id' variant="standard" value={params.milestone_id} onChange={(e) => {
        setParams({milestone_id: e.target.value})
      }}></TextField>
      <TextField label='state_event' variant="standard" value={params.state_event} onChange={(e) => {
        setParams({state_event: e.target.value})
      }}></TextField>
      <TextField label='title' variant="standard" value={params.title} onChange={(e) => {
        setParams({title: e.target.value})
      }}></TextField>
      

      <Button variant="outlined" color="success" onClick={() => {
        editIssue(localStorage.getItem('gitlabPAT') || '', projectId, id, params)
      }}>ویرایش</Button>
    </div>
  );
}
  
export default Issue;