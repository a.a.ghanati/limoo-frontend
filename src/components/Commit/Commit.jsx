import { Button } from "@mui/material";

function Commit({
  projectId,
  branch,
  id,
  message
}) {
  return (
    <div className='button-container'>
      <Button variant='outlined' href={`/projects/${projectId}/branches/${branch}/${id}`}>{message}</Button>
    </div>    
  );
}
  
export default Commit;