import { useState, useEffect } from 'react';
import { getIssues } from '../../helpers/requests';
import Issue from '../Issue/Issue';
import { Button } from "@mui/material";

function Issues({ id }) {
  const [issues, setIssues] = useState([])
  useEffect(() => getIssues(localStorage.getItem('gitlabPAT') || '', id).then((res) => setIssues(res.data)).catch(() => {}))

  return (
    <div id='container' dir='ltr'>
      <h2>Issues:</h2>
      
      {issues.map((issue) => (
        <Issue key={issue.iid} title={issue.title} description={issue.description} projectId={id} id={issue.iid} />
      ))}
      <div className='button-container'>
        <Button variant='contained' href={`/projects/${id}`}>بازگشت</Button>
      </div>
    </div>    
  );
}
  
export default Issues;