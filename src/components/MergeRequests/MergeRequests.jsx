import { useState, useEffect } from 'react';
import { getProjectMRs, getUser } from '../../helpers/requests';
import MergeRequest from '../MergeRequest/MergeRequest';
import { Button } from "@mui/material";

function MergeRequests({ id }) {
  const [MRs, setMRs] = useState([])
  const [username, setUsername] = useState('')
  useEffect(() => getProjectMRs(localStorage.getItem('gitlabPAT') || '', id).then((res) => setMRs(res.data)).catch(() => {}))
  useEffect(() => getUser(localStorage.getItem('gitlabPAT') || '').then((res) => setUsername(res.data.username)).catch(() => {}))

  return (
    <div id='container' dir='ltr'>
      <h2>Merge Requests:</h2>
      {MRs.map((MR) => <MergeRequest 
        key={MR.iid}
        title={MR.title} 
        description={MR.description} 
        mergable={MR.assignees.map((assignee) => assignee.username).includes(username)}
        projectId={id}
        iid={MR.iid}
      />)}
      <div className='button-container'>
        <Button variant='contained' href={`/projects/${id}`}>بازگشت</Button>
      </div>
    </div>    
  );
}
  
export default MergeRequests;