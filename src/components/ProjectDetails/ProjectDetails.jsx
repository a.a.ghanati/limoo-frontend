import { Button } from "@mui/material";

function ProjectDetails({
  id,
}) {
  return (
    <div id='container'>
      <div className='button-container'>
        <Button variant='outlined' href={`/projects/${id}/branches`}>Branches</Button>
      </div>
      <div className='button-container'>
        <Button variant='outlined' href={`/projects/${id}/merge-requests`}>Merge Requests</Button>
      </div>
      <div className='button-container'>
        <Button variant='outlined' href={`/projects/${id}/issues`}>Issues</Button>
      </div>
      <div className='button-container'>
        <Button variant='contained' href={`/projects`}>بازگشت</Button>
      </div>
    </div>
  );
}
  
export default ProjectDetails;