import { useState, useEffect } from 'react';
import { getBranchCommits } from '../../helpers/requests';
import Commit from '../Commit/Commit'
import { Button } from "@mui/material";

function Commits({ id, branch }) {
  const [commits, setCommits] = useState([])
  useEffect(() => getBranchCommits(localStorage.getItem('gitlabPAT') || '', id, branch).then((res) => setCommits(res.data)))

  return (
    <div id='container' dir='ltr'>
      <h2>Commits:</h2>
      {commits.map((commit) => <Commit key={commit.id} projectId={id} branch={branch} id={commit.id} message={commit.message} />)}
      <div className='button-container'>
        <Button variant='contained' href={`/projects/${id}/branches`}>بازگشت</Button>
      </div>
    </div>
  );
}
  
export default Commits;