import { Button, TextField } from "@mui/material";
import { useEffect, useState } from 'react';
import { getComments, postComment } from "../../helpers/requests";

function Comments({
  projectId,
  branchId,
  commitId
}) {
  const [comments, setComments] = useState([])
  const [comment, setComment] = useState('')
  useEffect(() => getComments(localStorage.getItem('gitlabPAT') || '', projectId, commitId)
  .then((res) => setComments(res.data)))

  return (
    <div id='container'>
      {comments.map((comment) => <div key={comment.created_at}>{comment.note}</div>)}
      <div>
        <div dir='rtl'>
          <TextField label='کامنت خود را وارد کنید:' variant="standard" value={comment} onChange={(e) => {
            setComment(e.target.value);
          }}/>
        </div>
        <div className='button-container'>
          <Button variant='contained' onClick={() => {
            postComment(localStorage.getItem('gitlabPAT') || '', projectId, commitId, comment)
            .then(() => {
              getComments(localStorage.getItem('gitlabPAT') || '', projectId, commitId)
              .then((res) => setComments(res.data))
            })
            .catch((err) => {
              console.log(err);
            })
          }}>
            ارسال کامنت
          </Button>
        </div>
      </div>
      <div className='button-container'>
        <Button variant='contained' href={`/projects/${projectId}/branches/${branchId}`}>بازگشت</Button>
      </div>
    </div>    
  );
}
  
export default Comments;