import { Button } from "@mui/material";

function Project({
  id,
  name
}) {
  return (
    <div className='button-container'>
      <Button variant='outlined' href={`/projects/${id}`}>{name}</Button>
    </div>
  );
}
  
export default Project;