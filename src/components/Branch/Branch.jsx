import { Button } from "@mui/material";

function Branch({
  projectId,
  name
}) {
  return (
    <div className='button-container'>
      <Button variant='outlined' href={`/projects/${projectId}/branches/${name}`}>{name}</Button>
    </div>
  );
}
  
export default Branch;