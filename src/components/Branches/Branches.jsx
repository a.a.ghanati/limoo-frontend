import Branch from "../Branch/Branch";
import { useState, useEffect } from 'react';
import { getProjectBranches } from '../../helpers/requests';
import { Button } from "@mui/material";

function Branches({ id }) {
  const [branches, setBranches] = useState([])
  useEffect(() => getProjectBranches(localStorage.getItem('gitlabPAT') || '', id).then((res) => setBranches(res.data)))

  return (
    <div id='container' dir='ltr'>
      <h2>Branches:</h2>
      {branches.map((branch) => <Branch key={branch.name} projectId={id} name={branch.name} />)}
      <div className='button-container'>
        <Button variant='contained' href={`/projects/${id}`}>بازگشت</Button>
      </div>
    </div>    
  );
}
  
export default Branches;