import { getProjects } from '../../helpers/requests';
import Project from "../Project/Project";
import { useState, useEffect } from 'react';
import { Button } from "@mui/material";

function Projects() {
  const [projects, setProjects] = useState([]);
  useEffect(() => getProjects(localStorage.getItem('gitlabPAT') || '').then((res) => setProjects(res.data)))

  return (
    <div id='container' dir='ltr'>
      <h2>Projects:</h2>
      {projects.map((project) => <Project key={project.id} id={project.id} name={project.name} />)}
      <div className='button-container'>
        <Button variant='contained' href={`/`}>بازگشت</Button>
      </div>
    </div>
  );
}
  
export default Projects;
  