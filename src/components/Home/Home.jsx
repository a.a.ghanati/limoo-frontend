import { useState } from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';

import { StyleSheetManager } from 'styled-components';
import rtlPlugin from 'stylis-plugin-rtl';
import { createTheme, ThemeProvider } from '@mui/material/styles';

function Home() {
  const [pat, setPat] = useState('')

  const theme = createTheme({
    direction: 'rtl',
  });

  return (
    <StyleSheetManager stylisPlugins={[rtlPlugin]}>
      <ThemeProvider theme={theme}>
        <div id='app'>
          <div dir='rtl'>
            <TextField label='کد دسترسی شخصی' variant="standard" value={pat} onChange={(e) => {
              localStorage.setItem('gitlabPAT', e.target.value);
              setPat(e.target.value);
            }}/>
          </div>
          <div className='button-container'>
            <Button variant='contained' href='/projects'>
              مشاهده لیست پروژه‌ها
            </Button>
          </div>
        </div>
      </ThemeProvider>
    </StyleSheetManager>
  );
}

export default Home;