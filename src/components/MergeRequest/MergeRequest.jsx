import { Button, Card } from "@mui/material";
import { merge } from '../../helpers/requests';

function MergeRequest({
  title,
  description,
  mergable,
  projectId,
  iid
}) {
  return (
    <Card>
      <h2>{title}</h2>
      <h4>{description}</h4>

      {mergable && <div className='button-container'>
        <Button variant='outlined' color="success" onClick={() => {
          merge(localStorage.getItem('gitlabPAT') || '', projectId, iid);
        }}>ادغام</Button>
      </div>}
    </Card>    
  );
}
  
export default MergeRequest;