import './App.css';
import Home from './components/Home/Home'
import Projects from './components/Projects/Projects';
import Branches from './components/Branches/Branches';
import Commits from './components/Commits/Commits';
import Comments from './components/Comments/Comments';
import ProjectDetails from './components/ProjectDetails/ProjectDetails';
import MergeRequests from './components/MergeRequests/MergeRequests';
import Issues from './components/Issues/Issues';

import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

function App() {
  return (
    <Router>
      <Switch>
          <Route path="/projects/:id/branches/:branch/:commit" render={({ match }) => (
            <Comments projectId={match.params.id} branchId={match.params.branch} commitId={match.params.commit} />
          )}></Route>

          <Route path="/projects/:id/branches/:branch" render={({ match }) => (
            <Commits id={match.params.id} branch={match.params.branch} />
          )}></Route>

          <Route path="/projects/:id/branches" render={({ match }) => (
            <Branches id={match.params.id} />
          )}></Route>

          <Route path="/projects/:id/merge-requests" render={({ match }) => (
            <MergeRequests id={match.params.id} />
          )}></Route>

          <Route path="/projects/:id/issues" render={({ match }) => (
            <Issues id={match.params.id} />
          )}></Route>

          <Route path="/projects/:id" render={({ match }) => (
            <ProjectDetails id={match.params.id} />
          )}></Route>
          
          <Route path="/projects">
            <Projects />
          </Route>
          
          <Route path="/">
            <Home />
          </Route>
        </Switch>
    </Router>
  );
}

export default App;
